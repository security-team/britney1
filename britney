#!/bin/bash

set -e
set -u

ulimit -d 8000000 -m 8000000 -v 8000000
umask 002


OPTIONS="$@"
qoption () {
    for a in $OPTIONS; do if [ "$a" = "$1" ]; then return 0; fi; done
    return 1
}

option () {
    for a in $OPTIONS; do if [ "$a" = "$1" ]; then date -uR; return 0; fi; done
    return 1
}

# DATE
NOW=`date +"%Y%m%d%H%M%S" -u`
YEAR=`date +"%Y" -u`

# Urgency cleanup; measured in days (find -mtime +$URGENCY_MAX_AGE)
URGENCY_MAX_AGE=35

# Dirs:
BASE=/srv/release.debian.org/britney
DAK_LOCKDIR=/srv/ftp-master.debian.org/lock
FTP_MIRROR=/srv/ftp.debian.org/mirror/ftp-master

SETS=/srv/release.debian.org/sets
DEBUG_SETS=${SETS}/testing-debug

D_I=$BASE/d-i
VAR=$BASE/var
SSH=${SETS}/ssh
INPUT=$BASE/input
CODE=$BASE/code/b1
STATE_DIR=$BASE/state
HINTS_DIR=$BASE/hints

LOCKDIR=$VAR/lock
HEIDI_SET=${SETS}/testing
SCRIPTS=$CODE/scripts
URGENCIES=$STATE_DIR/urgencies

LOCK=$LOCKDIR/britney.lock
DAK_LOCK=$DAK_LOCKDIR/daily.lock
DAK_STOP=$DAK_LOCKDIR/archive.stop

SSH_KEY=$SSH/dak_import_trigger_key

HTML=/srv/release.debian.org/www/britney

# Britney 2
DATA_B2=$VAR/data-b2
CODE_B2=$BASE/code/b2
B2_OUTPUT=$DATA_B2/output
B2_CONFIG=$CODE_B2/britney.conf
B2_CONFIG_NOBREAKALL=$CODE_B2/britney_nobreakall.conf

# Debci variables
# date --date $DEBCI_BACKLOG
DEBCI_BACKLOG='7 days ago'
DEBCI_HEADER_KEYFILE="$CODE/secrets/debci_key.header"
DEBCI_PRIORITY=4

if [ -f $DAK_STOP ]; then
  echo "$DAK_STOP exists, not running"
  exit 1
fi

# 10800 seconds = 3 hours
# 129600 seconds = 36 hours
if ! lockfile -r0 -l129600 $LOCK 2>/dev/null >/dev/null; then
    echo "Could not get britney lockfile!"
    ls -l "$LOCK"
    exit 1
fi

trap exit_function 0
exit_function () {
    rm -f $LOCK
}

eval $( dak admin config db-shell )

if ! qoption allowdaklock; then
	while [ -f $DAK_LOCK ]; do
		echo `date` $DAK_LOCK exists.  Sleeping in 10 more minutes.
		sleep 600
	done
fi

urgencies () {
  echo URGENCIES:
  cd $URGENCIES
  if ! wget --ca-directory=/etc/ssl/ca-debian -N -nv -r -np -nd -A 'install-urgencies-*' https://ftp-master.debian.org/britney/urgencies/ 2>&1 ; then
      echo "wget urgencies failed" >&2
      exit 1
  fi
  echo "Clean up old urgency files"
  find . -type f -name 'install-urgencies-*' -mtime "+${URGENCY_MAX_AGE}" -print -delete
  cd $BASE
  for u in $URGENCIES/install-urgencies-*; do
    [ -e "$u" ] || continue
    cat "$u" >> $1/age-policy-urgencies.new
  done
  mv -f  $1/age-policy-urgencies.new $1/age-policy-urgencies
}

if option urgencies; then
  urgencies $STATE_DIR
fi

bugs () {
  for suite in testing unstable; do
    x="$1/rc-bugs-${suite}"
    wget --ca-directory=/etc/ssl/ca-debian --quiet -O "${x}.new" https://bugs.debian.org/release-critical/britney/${suite}-nr
    if [ -s ${x}.new ]; then mv ${x}.new $x; else rm $x.new; exit 1; fi
  done
}
if option bugs; then
  echo BUGS:
  bugs $STATE_DIR
fi

piuparts () {
  status_file="$1/piuparts-status.txt"
  for suite in testing unstable; do
    x="$1/piuparts-summary-${suite}.json"
    wget --ca-directory=/etc/ssl/ca-debian --quiet "https://piuparts.debian.org/${suite}/summary.json" -O"${x}.new" || \
       echo "WARNING
Downloading piuparts results for $suite failed" | tee -a "${status_file}.new"
    if [ -s "${x}.new" ]; then mv -f "${x}.new" "$x" ; else rm "$x.new" ; fi
  done
  if [ -e "${status_file}.new" ]; then mv -f "${status_file}.new" "$status_file" ; else echo "OK
All piuparts results were downloaded" | tee "$status_file" ; fi
}

if option piuparts; then
  echo PIUPARTS:
  piuparts $STATE_DIR
fi

signers () {
  x="$1/signers.json"
  if $SCRIPTS/binary_signers.pl > ${x}.new
  then
    mv -f "${x}.new" "$x"
  else
    rm ${x}.new
  fi
}

if option signers; then
  echo SIGNERS:
  signers $STATE_DIR
fi

key_packages () {
    x="$1/key_packages.yaml"
    wget --ca-directory=/etc/ssl/ca-debian --quiet -O "${x}.new" https://udd.debian.org/cgi-bin/key_packages.yaml.cgi
    if [ -s ${x}.new ]; then mv ${x}.new $x; else rm $x.new; exit 1; fi
}
if option key_packages; then
  echo key packages download:
  key_packages $STATE_DIR
fi

_debci_assert_api_key() {
    if [ ! -f "${DEBCI_HEADER_KEYFILE}" ]; then
        echo "No API key for debci: Please verify/create ${DEBCI_HEADER_KEYFILE}" >&2
        # Do a stat for assist with debugging.
        stat "${DEBCI_HEADER_KEYFILE}" || :
        exit 1
    fi
}

debci_get () {
    _debci_assert_api_key
    # connect to debci and get all results
    curl --fail --silent --show-error \
         --header "@$DEBCI_HEADER_KEYFILE" \
         --cacert /etc/ssl/ca-global/ca-certificates.crt \
         --output $1/debci.json.new \
         "https://ci.debian.net/api/v1/test?since=$(date +%s --date="$DEBCI_BACKLOG")"
    if [ -s $1/debci.json.new ] ; then mv $1/debci.json.new $1/debci.json ; else exit 1 ; fi
}

if option debci_get; then
  echo DEBCI download:
  debci_get $STATE_DIR
fi

run_b2 () {
  if qoption run-excuses-only ; then
    $CODE_B2/britney.py -c $B2_CONFIG -v --no-compute-migrations
  else
    $CODE_B2/britney.py -c $B2_CONFIG -v
  fi
}

if option run || option run-excuses-only ; then
  echo RUN:
  run_b2
fi

debci_put () {
    _debci_assert_api_key
    # connect to debci and request tests
    MYFILE=debci.amqp
    find $1/$2 2> /dev/null | while read INFILE ; do
        SUITE=${INFILE#*/debci_}
        SUITE=${SUITE%.input}
        mv $INFILE $1/$MYFILE
        while [ -s $1/$MYFILE ] ; do
            $SCRIPTS/britney2debci.py $1 $MYFILE
            for FILE in $1/debci_submit_*.json ; do
                ARCH=${FILE#*/debci_submit_}
                ARCH=${ARCH%.json}
                curl --fail --silent --show-error \
                     --header "@$DEBCI_HEADER_KEYFILE" \
                     --cacert /etc/ssl/ca-global/ca-certificates.crt \
                     --form priority=$DEBCI_PRIORITY \
                     --form tests=@$FILE \
                     "https://ci.debian.net/api/v1/test/$SUITE/$ARCH"
                # Make sure we don't submit this multiple times by accident
                rm $FILE
            done
        done
    done
}

if option debci_put; then
  echo DEBCI upload:
  debci_put $B2_OUTPUT debci_*.input
fi

dak_import() {
  # let dak import (control-suite) the new set
  md5=$(md5sum $HEIDI_SET/current | awk '{print $1}')
  ssh -2 -T -o BatchMode=yes -i $SSH_KEY dak@fasolo.debian.org import_dataset.sh testing ${md5} < $HEIDI_SET/current
}

save () {
  cd $BASE
  echo RESULTS:
  # write out a new heidi set
  HEIDI_RESULT="$B2_OUTPUT/HeidiResult"
  echo Using data from $HEIDI_RESULT
  HEIDI_FILENAME=$HEIDI_SET/`date -u +"%Y-%m-%d_%H.%M"`
  HEIDI_CURRENT=$HEIDI_SET/current
  (grep -Evi ' faux$' $HEIDI_RESULT | cut -d\  -f1-3 | sort -u) >$HEIDI_FILENAME
  gzip --best `readlink -f $HEIDI_CURRENT`
  ln -sf `basename $HEIDI_FILENAME` $HEIDI_CURRENT
  dak_import
  # update the release mirror on ries
  # printf "Updating DD-accessible mirror..."
  # ssh -T release-sync-push || true
  # printf " done\n"
}

if option save; then
  save b2
fi

if option dak_import; then
  dak_import
fi

dak_import_debug () {
  local heidi_file heidi_symlink debug_ssh_key
  heidi_file=${DEBUG_SETS}/$(date +%Y-%m-%d_%H.%M)
  heidi_symlink=${DEBUG_SETS}/current
  debug_ssh_key=${SSH}/dak_import_trigger_key
  if ! echo "
        WITH binaries_suite_arch AS (
	        SELECT bin_associations.id, binaries.id AS bin, binaries.package, binaries.version, binaries.source, bin_associations.suite,
                    suite.suite_name, binaries.architecture, architecture.arch_string
                FROM (((binaries JOIN bin_associations ON ((binaries.id = bin_associations.bin))) JOIN suite ON ((suite.id = bin_associations.suite))) JOIN
                    architecture ON ((binaries.architecture = architecture.id)))
        )
        SELECT debug.package || ' ' || debug.version || ' ' || arch.arch_string
        FROM binaries debug, architecture arch, binaries_suite_arch testing, files_archive_map fam, archive
        WHERE arch.id = debug.architecture
          AND testing.suite_name = 'testing'
          AND debug.package = testing.package || '-dbgsym'
          AND debug.version = testing.version
          AND debug.architecture = testing.architecture
          AND archive.id = fam.archive_id
          AND fam.file_id = debug.file
          AND archive.name = 'debian-debug'
        ORDER BY debug.package;" | psql -v ON_ERROR_STOP=1 -A -t service=projectb > $heidi_file
  then
      echo "Getting dbgsym list from projectb failed. Aborting dak_import_debug" >&2
      echo "but continuing with the rest of the tasks." >&2
      return 0
  fi
  ln -sf $heidi_file $heidi_symlink
  md5=$(md5sum $heidi_file | awk '{print $1}')
  ssh -T -o BatchMode=yes -i $debug_ssh_key dak@fasolo.debian.org import_dataset.sh testing-debug ${md5} < $heidi_file

  # clean-up old files
  find ${DEBUG_SETS} -type f -mtime +7 -delete
}

if option dbgsym; then
  dak_import_debug
fi


create_uninst_report () {
    $CODE_B2/britney.py -c $1 --print-uninst >$HTML/$2.new
    echo -e "\n# Generated: `date -uR`" >>$HTML/$2.new
    mv $HTML/$2.new $HTML/$2
}

stats () {
  echo STATS:
  cp $B2_OUTPUT/excuses*.yaml $HTML/
  cp $B2_OUTPUT/excuses_*.html $HTML/
  # historically the output had a completely different name
  cp $B2_OUTPUT/excuses.html $HTML/update_excuses.html
  cp $B2_OUTPUT/output.txt $HTML/update_output.txt
  cp $B2_OUTPUT/pseudo-excuses-experimental.* $HTML/
  gzip -9n < $HTML/excuses.yaml > $HTML/excuses.yaml.gz
  gzip -9n < $HTML/update_excuses.html > $HTML/update_excuses.html.gz
  gzip -9n < $HTML/update_output.txt > $HTML/update_output.txt.gz
  mkdir -p $HTML/update_output/$YEAR
  mkdir -p $HTML/update_excuses/$YEAR
  cp $HTML/update_output.txt.gz $HTML/update_output/$YEAR/$NOW.txt.gz
  cp $HTML/update_excuses.html.gz $HTML/update_excuses/$YEAR/$NOW.html.gz
  cp $HTML/excuses.yaml.gz $HTML/update_excuses/$YEAR/$NOW.yaml.gz

  # TODO: {stable,unstable}_uninst.txt -- needs support in b2, talk to Fabio
  # TODO: this creates uninstallability reports against b2 results, not b1's
  create_uninst_report $B2_CONFIG testing_uninst.txt
  create_uninst_report $B2_CONFIG_NOBREAKALL testing_uninst_full.txt

  if grep -q -e '-meta-faux' $HTML/testing_uninst.txt; then
    echo >&2 'Warning! Some -meta-faux package is uninstallable!'
  fi

}

if option stats; then
  stats b2
fi

if option summary; then
  cd $BASE
  #echo "Out of dates holding up testing:"
  #TERM=vt100 lynx -nolist -dump $HTML/update_excuses.html | sed -n 's/^ *[^ ] *[^ ]*out of date on \([^ ]*\): .*$/\1/p' | sort | uniq -c | sort -n
  echo "Uninstallables holding up testing:"
  sed < $HTML/update_excuses.html -n 's/^ *<li>[^ ]* (\([^, ]*\),.*) uninstallable.*$/\1/p' | sort | uniq -c | sort -n
  #echo "wanna-build stats:"
  #for a in alpha arm hppa hurd-i386 i386 ia64 m68k mips mipsel powerpc s390 sparc; do
  #  printf "  %-12s " "$a:"
  #  /srv/wanna-build/bin/wanna-build-statistics --database=$a/build-db | 
  #    grep "if also counting" || echo "-"
  #done
fi

if option updatehtml; then
    rsync -aL --delete $STATE_DIR/ $HTML/state/
    rsync -aL --delete $HINTS_DIR/ $HTML/hints/
    cp -a $B2_CONFIG $HTML/britney.conf
fi

if option synchtml; then
    if ! static-update-component release.debian.org 2>&1 ; then
        echo "static-update-component release.d.o failed" >&2
        exit 1
    fi
fi

echo -n "Finished at: "; date -uR
