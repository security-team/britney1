#! /usr/bin/python
## encoding: utf-8
#
# Copyright (c) 2008 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

"""Handle the creation of britney faux packages.

This program gets called from the "britney" script in order to append to the
Packages_<arch> files a list of faux packages. This is done with the "generate"
command, passing a list of britney suite directories:

  % fauxpkg.py generate /home/release/britney/var/data/{testing,unstable}

This automatically appeds to the Packages files the list of faux packages. See
the README file in this directory for the input files from which such list is
generated.
"""

import os
import re
import sys
import glob
import tempfile
import subprocess

import apt_pkg
apt_pkg.init()

##

BASEDIR = os.path.dirname(__file__)

NOREMOVE_DIR =  os.path.join(BASEDIR, 'noremove.d')
FAUX_PACKAGES = os.path.join(BASEDIR, 'FauxPackages')

DEFAULT_NOREMOVE_ARCH = 'i386'

##

def main():
    if not sys.argv[1:]:
        print >>sys.stderr, 'Usage: %s <generate | update-tasksel> [ britney_suite_dir1 ... ]' % (
                os.path.basename(sys.argv[0]))
        sys.exit(1)
    else:
        command = sys.argv.pop(1)

    if command == 'generate':
        if not sys.argv[1:]:
            print >>sys.stderr, 'E: need at least one britney suite directory'
            sys.exit(1)
        else:
            do_generate(sys.argv[1:])
    elif command == 'update-tasksel':
        if sys.argv[1:]:
            print >>sys.stderr, 'E: extra arguments not allowed'
            sys.exit(1)
        else:
            do_update_tasksel()
    else:
        print >>sys.stderr, 'E: unknown command %s' % (command,)

##

def do_generate(directories):
    arches = set()
    allfaux = {}

    for dir_ in directories:
        arches.update([ re.sub(r'^.*/Packages_', '', x)
            for x in glob.glob(os.path.join(dir_, 'Packages_*')) ])

    # First, FauxPackages
    try:
        parser = apt_pkg.TagFile(file(FAUX_PACKAGES))
        step = parser.step
        section = parser.section
    except AttributeError, e:
        parser = apt_pkg.ParseTagFile(file(FAUX_PACKAGES))
        step = parser.Step
        section = parser.Section
    while step():
        d = dict(section)
        d['Section'] = 'faux' # crucial; britney filters HeidiResult based on section

        if not d.has_key('Architecture'):
            these_arches = arches
        else:
            these_arches = set(re.split(r'[, ]+', d['Architecture']))

        d['Architecture'] = 'all' # same everywhere

        for arch in these_arches:
            allfaux.setdefault(arch, []).append(d)

    # Now, noremove.d
    for f in glob.glob(os.path.join(NOREMOVE_DIR, '*.list')):
        pkgs = {}
        basename = re.sub(r'.+/(.+)\.list', r'\1', f)

        for line in file(f):
            line = line.strip()
            if re.match(r'^#', line):
                continue
            elif re.match(r'\S+$', line):
                pkg = line
                arch = DEFAULT_NOREMOVE_ARCH
            else:
                m = re.match(r'(\S+)\s+\[(.+)\]', line)
                if m:
                    pkg, arch = m.groups()
                else:
                    print >>sys.stderr, 'W: could not parse line %r' % (line,)

            arch = re.split(r'[, ]+', arch)[0] # just in case
            pkgs.setdefault(arch, set()).add(pkg)

        for arch in pkgs.keys():
            d = { 'Package': '%s-meta-faux' % (basename,), 'Version': '1',
                  'Section': 'faux', 'Architecture': '%s' % (arch,),
                  'Depends': ', '.join(pkgs[arch]) }
            allfaux.setdefault(arch, []).append(d)

    # Write the result
    for arch in arches:
        for dir_ in directories:
            f = os.path.join(dir_, 'Packages_' + arch)
            if not os.path.exists(f):
                continue
            else:
                f = file(f, 'a')
                for d in allfaux[arch]:
                    f.write('\n'.join('%s: %s' % (k, v) for k, v in d.iteritems()) + '\n\n')

##

def do_update_tasksel():
    p = subprocess.Popen('dak ls -f control-suite -s unstable -a source tasksel',
            shell=True, stdout=subprocess.PIPE)
    p.wait()
    version = p.stdout.readline().split()[1]

    p = subprocess.Popen('dak ls -f control-suite -s unstable -S -a i386,all tasksel',
            shell=True, stdout=subprocess.PIPE)
    p.wait()
    tasks = []

    for line in p.stdout:
        pkg = line.split()[0]

        if pkg.startswith('task-'):
            tasks.append(pkg)

    # Write the new file
    tmpfd, tmpname = tempfile.mkstemp(dir=NOREMOVE_DIR)
    os.write(tmpfd, '# Generated from tasksel-data %s\n' % (version,))
    os.write(tmpfd, '\n'.join(sorted(tasks)) + '\n')
    os.close(tmpfd)
    os.chmod(tmpname, 0644)
    os.rename(tmpname, os.path.join(NOREMOVE_DIR, 'tasksel.list'))

##

if __name__ == '__main__':
    main()
